//импортируем функцию RTree дерева
import RTree from '../cesiumgs-modified/Source/example.js'

//определяем класс визуализации Rtree дерева
class RTreeView
{
    //обьект функции RTree дерева
    tree;

    //полные данные из json-файла
    jsonData;

    //массив прямоугольников RTree дерева
    #rects;

    //обозреватель cesium
    #viewer;

    //сущность обозревателя cesium
    #entities;

    //размер одной буквы по горизонтали в градусах
    #dx;

    //фактор масштабирования между вертикальным и горизонтальным размером каждой буквы
    #scaleHSign;

    //размер одной буквы по вертикали в градусах
    #dy;

    //общий фактор масштабирования каждой буквы как по горизонтали так и по вертикали
    scaleFont;

    constructor() 
    {
        //задаем обьект функции RTree дерева
        this.tree = RTree();

        //задаем начальный вид массива прямоугольников RTree дерева
        this.rects = [];

        //определяем общий фактор масштабирования каждой буквы как по горизонтали так и по вертикали
        this.scaleFont = document.getElementById('scaleFont').value;

        //задаем размер одной буквы по горизонтали в градусах
        this.dx         = 0.00010238 ;

        // задаем фактор масштабирования между вертикальным и горизонтальным размером каждой буквы
        this.scaleHSign = 1.2036;

        //определяем общий фактор масштабирования каждой буквы как по горизонтали так и по вертикали
        this.dy         = this.scaleHSign*this.dx ;
    }

    //функция скачивания информации о координатах из файла db.json c помощью модуля json-server
    #checkUserHosting = async (hostFeatures) => 
    {
        let hostFeaturesData  = await fetch(`http://localhost:5000/${hostFeatures}`)
        let hostFeaturesJson = await hostFeaturesData.json();
        return hostFeaturesJson;
    }

    //функция скачивания информации при помощи модуля json-server при get запросе с адреса http://localhost:5000/features
    #genJsonData = async () =>
    {
        return await this.#checkUserHosting("features");
    }

    #genRectData = () => {
        //задаем массив значений координат
        var rectData = [];

        //формируем массив значений координат и названий населенных пунктов в этих координатах 
        this.jsonData.map((item) => {
            //размер названия в символах населенного пункта в координатах
            //item.geometry.coordinates[0][0] и item.geometry.coordinates[0][1] 

            if (item.properties.feature_name !== undefined && item.properties.feature_name !== null)
            {
                rectData .push({
                    "x" : item.geometry.coordinates[0][0], 
                    "y" : item.geometry.coordinates[0][1],
                    "w" : this.dx*item.properties.feature_name.length,
                    "h" : this.dy,
                    "name" : item.properties.feature_name
                })          
            }
        })

        return rectData;
    }

    //функция вытаскивания прямоугольника из узла дерева прямоугольников
    #generateRect(node) 
    {
        if (!node) { return; }

        //фиксируем вытащенный прямоугольник
        this.rects.push({ x: node.x, y: node.y, w: node.w*this.scaleFont, h: node.h*this.scaleFont, name: node.name});

        if ('leaf' in node) return;

        //продолжаем дальше перебирать узлы дерева
        for (var i = 0; i < node.nodes.length; i++) 
        {
            this.#generateRect(node.nodes[i]);
        }
    }

    //функция формирования массива прямоугольников имеющегося RTree дерева
    generateRects() 
    {
        //составляем массив прямоугольников сформированного дерева прямоугольников
        this.rects = [];

        this.#generateRect(this.tree.root);

        //определяем обозреватель cesium
        this.viewer = new Cesium.Viewer("cesiumContainer");

        //определяем сущность обозревателя cesium
        this.entities = this.viewer.entities;

        //изображаем массив прямоугольников в Cesium
        if (this.rects.length > 0)
        {
            for (var i = 0; i <= this.rects.length - 1 ; i++)
            {
                this.entities.add({
                    polygon: 
                    {
                        hierarchy: new Cesium.PolygonHierarchy(
                            Cesium.Cartesian3.fromDegreesArray([
                                this.rects[i].x , this.rects[i].y ,
                                this.rects[i].x + this.rects[i].w , this.rects[i].y ,
                                this.rects[i].x + this.rects[i].w , this.rects[i].y + this.rects[i].h,
                                this.rects[i].x , this.rects[i].y + this.rects[i].h ,
                                this.rects[i].x , this.rects[i].y
                            ])
                        ),
                        material: Cesium.Color.BLUE.withAlpha(0.25),
                        classificationType: Cesium.ClassificationType.BOTH
                    },
                    externalId: [-1]
                })
            }
        }  

        this.viewer.zoomTo(this.viewer.entities);  
    }

    //функция представления прямоугольников образовавшегося Rtree дерева в обозревателе cesium
    fullRepresentingRTree = async() => 
    {
        //определяем данные координат по которым будем составлять дерево прямоугольников 
        this.jsonData = await this.#genJsonData();

        var rectData = this.#genRectData();
        
        //составляем дерево прямоугольников
        for (var i = 0; i < rectData.length; i++) 
        {
            this.tree.insert(rectData[i]);
        }

        //вытаскиваем прямоугольники из составленного дерева прямоугольников
        this.generateRects();
    }
}

//класс поиска прямоугольника по дереву прямоугольников
class TaskSearchOnRTree extends RTreeView 
{
    //массив найденных прямоугольников RTree дерева которые содержат заданный прямоугольник
    #searchedRects;    

    constructor() 
    {
        super();
        //задаем начальный вид массива найденных прямоугольников RTree дерева которые содержат заданный прямоугольник
        this.searchedRects = [];
    }  

    //функция поиска ректа в узле имеющегося дерева ректов по заданным координатам
    #searchingRects = (node, lon1, lat1, lon2, lat2) =>
    {
        if (!node) { return; }

        if (node.x > lon1 && node.y > lat1 && node.x + node.w*this.scaleFont < lon2 && node.y + node.h*this.scaleFont < lat2)
        {
            //требуемый рект найден
            taskSearchOnRTree.searchedRects.push({ x: node.x, y: node.y, w: node.w*this.scaleFont, h: node.h*this.scaleFont, name: node.name});
        }

        if ('leaf' in node) return;

        //продолжаем требуемый поиск по другим узлам дерева
        for (var i = 0; i < node.nodes.length; i++) 
        {
            this.#searchingRects(node.nodes[i], lon1, lat1, lon2, lat2);
        }
    }

    //функция вывода рузультата, который касается поиска прямоугольников в каждом из которых находится заданный прямоугольник
    #outputSearchResult = (dataBegin, dataEnd, quantRect) => 
    {
        if (quantRect > 0) 
        {
            //результат проведенного поиска
            document.getElementById('outputSearchResult').innerHTML = 
            quantRect + ' rects was founded during ' + (dataEnd - dataBegin) + ' ms';
        }
    }

    //функция осуществления требуемого поиска
    performingSearching = () => {
        //определяем правые верхние координаты заданного прямоугольника
        let lon1 = document.getElementById('lon1').value;
        let lat1 = document.getElementById('lat1').value;
        //определяем левые нижние координаты заданного прямоугольника
        let lon2 = document.getElementById('lon2').value;
        let lat2 = document.getElementById('lat2').value;

        this.searchedRects = [];  
        //фиксируем время начала поиска
        var dataBegin = (new Date()).valueOf();
        //проводим требуемый поиск по имеющемуся дереву прямоугольников
        this.#searchingRects(this.tree.root, lon1, lat1, lon2, lat2);
        //фиксируем время окончания поиска
        var dataEnd = (new Date()).valueOf();

        //выводим результат, который касается проведенного поиска
        this.#outputSearchResult(dataBegin, dataEnd, this.searchedRects.length);

        //в консоль выводим информацию о ректах в которых находится заданный прямоугольник
        console.log("this.searchedRects");
        console.log(this.searchedRects);
    }
}

//определяем переменную класса TaskSearchOnRTree
var taskSearchOnRTree = new TaskSearchOnRTree();

// осуществляем презентацию RTree дерева в Cesium
taskSearchOnRTree.fullRepresentingRTree();

//функция - обработчик требуемого поиска 
document.getElementById('searching').onclick = function () 
{
    //осуществляем требуемый поиск
    taskSearchOnRTree.performingSearching();
};

//функция - обработчик перерисования ректов с новым масштабированием
document.getElementById('updating').onclick = function () 
{
    //информацию о старом поиске очищаем
    document.getElementById('outputSearchResult').innerHTML = "";

    //переопределяем общий размер масштабирования каждой буквы как по горизонтали так и по вертикали
    taskSearchOnRTree.scaleFont = document.getElementById('scaleFont').value;

    //проводим соответствующую презентацию с переопределенным коэффициентом масштабирования букв scaleFont         
    taskSearchOnRTree.generateRects();
};