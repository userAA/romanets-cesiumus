
import RTree from './RTree/lib/rtree.js';
import geojson from './RTree/lib/geojson.js';

//RTree.prototype.bbox = geojson.bbox;
RTree.prototype.geoJSON = geojson.geoJSON;

import Rectangle from './RTree/lib/rectangle.js';
RTree.Rectangle = Rectangle;

export default RTree;